package com.stepanov.googlesearch.activities;

import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

import com.stepanov.googlesearch.R;
import com.stepanov.googlesearch.base.BaseApplication;
import com.stepanov.googlesearch.fragments.SearchFragment;
import com.stepanov.googlesearch.loaders.GoogleSearchResultsAsyncTaskLoader;
import com.stepanov.googlesearch.models.SearchResult;
import com.stepanov.googlesearch.network.SearchApi;

import java.util.List;


public class SearchActivity extends ActionBarActivity implements LoaderManager.LoaderCallbacks<List<SearchResult>>, SearchFragment.Callback {

    private static final int LOADER_SEARCH_RESULTS_ID = 1;

    private SearchFragment searchFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        getSupportActionBar().hide();
        if (savedInstanceState == null) {
            searchFragment = new SearchFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, searchFragment)
                    .commit();
        }
    }

    @Override
    public Loader<List<SearchResult>> onCreateLoader(int id, Bundle args) {
        Loader<List<SearchResult>> loader = null;
        if (id == LOADER_SEARCH_RESULTS_ID) {
            loader = new GoogleSearchResultsAsyncTaskLoader(this, getApi(), args);
        }
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<List<SearchResult>> listLoader, List<SearchResult> searchResults) {
        setResults(searchResults);
    }

    @Override
    public void onLoaderReset(Loader<List<SearchResult>> listLoader) {
        // TODO
    }

    @Override
    public void search(String query) {
        Bundle bndl = new Bundle();
        bndl.putString(GoogleSearchResultsAsyncTaskLoader.QUERY, query);
        Loader<List<SearchResult>> loader = getSupportLoaderManager().restartLoader(LOADER_SEARCH_RESULTS_ID, bndl, this);
        loader.forceLoad();
    }

    private void setResults(List<SearchResult> searchResults) {
        searchFragment.onSearchResultsLoaded(searchResults);
    }

    private SearchApi getApi() {
        BaseApplication baseApplication = (BaseApplication) getApplication();
        return baseApplication.getApi();
    }

}
