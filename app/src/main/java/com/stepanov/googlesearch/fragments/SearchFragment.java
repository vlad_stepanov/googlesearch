package com.stepanov.googlesearch.fragments;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.stepanov.googlesearch.R;
import com.stepanov.googlesearch.adapters.SearchResultsAdapter;
import com.stepanov.googlesearch.models.SearchResult;

import java.util.List;

/**
 * Created by Vlad on 05.09.2014.
 */
public class SearchFragment extends Fragment {

    public interface Callback {
        public void search(String query);
    }

    private EditText searchEditText;
    private ListView listView;
    private ProgressBar progressBar;

    private Callback callback;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_search, container, false);
        searchEditText = (EditText) rootView.findViewById(R.id.search_edit_text);
        listView = (ListView) rootView.findViewById(R.id.list);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progress_bar);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ListAdapter listAdapter = listView.getAdapter();
                SearchResult searchResult = (SearchResult) listAdapter.getItem(position);
                openLink(searchResult.link);
            }
        });

        rootView.findViewById(R.id.search_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String searchQuery = searchEditText.getText().toString();
                search(searchQuery);
            }
        });

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof Callback) {
            callback = (Callback) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callback = null;
    }

    public void onSearchResultsLoaded(List<SearchResult> searchResults) {
        progressBar.setVisibility(View.GONE);
        if (searchResults == null) return;
        listView.setAdapter(new SearchResultsAdapter(getActivity(), searchResults));
    }

    private void search(String searchString) {
        if (callback != null) {
            callback.search(searchString);
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    private void openLink(String link) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        startActivity(browserIntent);
    }

}

