package com.stepanov.googlesearch.models;

/**
 * Created by Vlad on 05.09.2014.
 */
public class SearchResult {

    public String title;
    public String link;
    public String snippet;

    public SearchResult() {
    }

    public SearchResult(String title, String link, String snippet) {
        this.title = title;
        this.link = link;
        this.snippet = snippet;
    }

}
