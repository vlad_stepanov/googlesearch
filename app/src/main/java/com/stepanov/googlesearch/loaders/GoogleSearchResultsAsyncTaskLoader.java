package com.stepanov.googlesearch.loaders;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.AsyncTaskLoader;

import com.stepanov.googlesearch.models.SearchResult;
import com.stepanov.googlesearch.network.SearchApi;

import java.util.List;

/**
 * Created by Vlad on 05.09.2014.
 */
public class GoogleSearchResultsAsyncTaskLoader extends AsyncTaskLoader<List<SearchResult>> {

    public static final String QUERY = "query";

    private SearchApi searchApi;
    private String query;

    public GoogleSearchResultsAsyncTaskLoader(Context context, SearchApi searchApi, Bundle args) {
        super(context);
        this.searchApi = searchApi;
        if (args != null)
            query = args.getString(QUERY);
    }

    @Override
    public List<SearchResult> loadInBackground() {
        return searchApi.search(query);
    }

}
