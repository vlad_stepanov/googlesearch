package com.stepanov.googlesearch.network;

import android.content.Context;
import android.util.Log;

import com.stepanov.googlesearch.models.SearchResponse;
import com.stepanov.googlesearch.models.SearchResult;

import java.net.URLEncoder;
import java.util.List;

/**
 * Created by Vlad on 05.09.2014.
 */
public class SearchApi {

    private static final String TAG = "SearchApi";
    private static final String GOOGLE_SEARH_API_KEY = "AIzaSyB9mAhx7W7mze0AzB1M6ctfzuI0F7TIwXs";
    private static final String GOOGLE_SEARH_API_CX = "004236265029127643878:qdnhrf6sg24";
    private static final String GOOGLE_SEARH = "https://www.googleapis.com/customsearch/v1" +
            "?key=" + GOOGLE_SEARH_API_KEY +
            "&cx=" + GOOGLE_SEARH_API_CX +
            "&q=";


    private final Client client;

    public SearchApi(Context context) {
        client = new Client(context);
    }

    public List<SearchResult> search(String query) {
        try {
            String encodedQuery = URLEncoder.encode(query, "utf-8");
            SearchResponse response = client.get(GOOGLE_SEARH + encodedQuery, SearchResponse.class);
            return response == null ? null : response.items;
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return null;
    }

    public void close() {
        client.close();
    }
}
