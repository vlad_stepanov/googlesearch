package com.stepanov.googlesearch.network;

import android.content.Context;
import android.net.http.AndroidHttpClient;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.concurrent.Semaphore;

/**
 * Created by Vlad on 05.09.2014.
 */
public class Client {

    private static final int DEFAULT_CONNECTION_TIMEOUT = 10000;
    private static final int DEFAULT_SO_TIMEOUT = 12000;

    private final AndroidHttpClient client;
    private final Semaphore semaphoreClient = new Semaphore(1);
    private final Gson gson = new Gson();


    public Client(Context context) {
        client = AndroidHttpClient.newInstance("Android_client", context);
        client.enableCurlLogging("HTTPClient", android.util.Log.WARN);
        HttpConnectionParams.setConnectionTimeout(client.getParams(), DEFAULT_CONNECTION_TIMEOUT);
        HttpConnectionParams.setSoTimeout(client.getParams(), DEFAULT_SO_TIMEOUT);

    }

    public <T> T get(String url, Class<T> type) throws ServerErrorException, IOException {
        HttpGet httpRequest = new HttpGet(url);
        return execute(httpRequest, type);
    }

    private <T> T execute(HttpUriRequest request, Class<T> type) throws ServerErrorException, IOException {
        HttpResponse response = null;
        try {
            semaphoreClient.acquire();
            response = client.execute(request);
        } catch (InterruptedException e) {
            Log.e("Client.execute()", e.getMessage(), e);
        } catch (IOException e) {
            throw e;
        } finally {
            semaphoreClient.release();
        }
        if (response == null) {
            throw new ServerErrorException("Null response", 0);
        }
        String responseString = getResponseAsString(response);
        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            if (type != null) {
                if (responseString != null && !TextUtils.isEmpty(responseString)) {
                    synchronized (gson) {
                        Log.i("Client", responseString);
                        return gson.fromJson(responseString, type);
                    }
                }
            }
            consumeEntity(response);
        } else {
            consumeEntity(response);
            int statusCode = response.getStatusLine().getStatusCode();
            throw new ServerErrorException("Server error: " + statusCode + ", "
                    + response.getStatusLine().getReasonPhrase()
                    + ". " + responseString, statusCode);
        }
        return null;
    }

    private void consumeEntity(HttpResponse response) throws IOException {
        HttpEntity entity = response.getEntity();
        if (entity != null) {
            entity.consumeContent();
        }
    }

    private String getResponseAsString(HttpResponse resp) throws IOException {
        StringBuilder builder = new StringBuilder();
        HttpEntity entity = resp.getEntity();
        if (entity != null) {
            builder.append(EntityUtils.toString(entity, HTTP.UTF_8));
        }
        return builder.toString();
    }

    public void close() {
        client.getConnectionManager().shutdown();
    }

}
