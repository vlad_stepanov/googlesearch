package com.stepanov.googlesearch.network;

/**
 * Created by Vlad on 05.09.2014.
 */
public class ServerErrorException extends Exception {

    private int errorCode;

    public ServerErrorException(String message, int errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }

}
