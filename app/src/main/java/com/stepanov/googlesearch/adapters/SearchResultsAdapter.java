package com.stepanov.googlesearch.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.stepanov.googlesearch.R;
import com.stepanov.googlesearch.models.SearchResult;

import java.util.List;

/**
 * Created by Vlad on 05.09.2014.
 */
public class SearchResultsAdapter extends BaseAdapter {

    private class Holder {

        TextView titleTextView;
        TextView urlTextView;
        TextView snippetTextView;

        Holder(View view) {
            titleTextView = (TextView) view.findViewById(R.id.title);
            urlTextView = (TextView) view.findViewById(R.id.link);
            snippetTextView = (TextView) view.findViewById(R.id.snippet);
        }

        public void fill(SearchResult searchResult) {
            titleTextView.setText(searchResult.title);
            urlTextView.setText(searchResult.link);
            snippetTextView.setText(searchResult.snippet);
        }

    }

    private LayoutInflater layoutInflater;
    private List<SearchResult> searchResults;

    public SearchResultsAdapter(Context context, List<SearchResult> searchResults) {
        this.searchResults = searchResults;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_item_search, null);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        holder.fill(getItem(position));

        return convertView;
    }

    @Override
    public int getCount() {
        return searchResults.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public SearchResult getItem(int position) {
        return searchResults.get(position);
    }

}
