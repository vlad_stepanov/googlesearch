package com.stepanov.googlesearch.base;

import android.app.Application;

import com.stepanov.googlesearch.network.SearchApi;

/**
 * Created by Vlad on 05.09.2014.
 */
public class BaseApplication extends Application {

    private SearchApi api;

    public SearchApi getApi() {
        if (api == null) {
            api = new SearchApi(this);
        }
        return api;
    }

    @Override
    public void onTerminate() {
        getApi().close();
    }

}
